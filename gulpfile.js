var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

// Compile SCSS/SASS
gulp.task('sass', gulp.series(function(){
	return gulp.src('resources/scss/app.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('assets/css'));
}));

// Minify JS
gulp.task('scripts', function() {
  return gulp.src('resources/js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('assets/js'))
});

// Watch
gulp.task('watch', gulp.series(function(){
	gulp.watch('resources/scss/*.scss', gulp.parallel(['sass']));
	gulp.watch('resources/js/*.js', gulp.parallel(['scripts']));
}));

// Task Default
gulp.task('default', gulp.series(['sass', 'watch']));