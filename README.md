## Configurando e utilizando o projeto
Rodar o 'npm install' para instalar as dependências do GULP. Os arquivos brutos estão na pasta /resources.

## Configs
GULP - CLI 2.2.0 / Local 4.0.1
Bootstrap - 4.3.1
FontAwesome - 5.8.1
jQuery - 1.11.2

## Branches
Master - Finalizado / Release - Teste / DEV - Desenvolvimento

## Sobre o Projeto
Card para intranet dos heróis da DC. O herói escolhido foi o Batman e cor primária é: FFF200. Com essa base nessa cor escolhi a patela de cores do Batman Clássico, com tons de cinza, azul e o amarelo escolhido.

## EXCELSIOR
Stan Lee que me perdoe, mas foi a agência quem pediu pra ser da DC.