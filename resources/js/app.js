$(document).ready(function(){
	// Show & Hide response
	$('.form').on('submit', function(){
		var t = $(this);
		var td = t.data('send');
		var position = $('#profile').offset().top;

		$('.r_'+td).fadeIn('fast');

		$('html').animate({ scrollTop: position }, function(){
			$('#'+td).fadeOut('fast');
		});

		return false;
	});

	$('.closeresponse').on('click', function(){
		var t = $(this);
		var td = t.data('response');

		$('.r_'+td).fadeOut('fast');
	});

	// Show & Hide forms
	$('.openform').on('click', function(){
		var t = $(this);
		var td = t.data('form');

		$('#'+td).fadeIn('fast', function(){
			var position = $('#'+td).offset().top;
			$('html').animate({ scrollTop: position });
		});
	});

	$('.closeform').on('click', function(){
		var t = $(this);
		var td = t.data('form');
		var position = $('#profile').offset().top;

		$('html').animate({ scrollTop: position }, function(){
			$('#'+td).fadeOut('fast');
		});
	});

	// Campo Outros
	$('#formacessorios').find('input[type=checkbox][value=outro]').on('click', function(){
		var t = $(this);

		if(t.prop('checked')) {
			t.nextAll().eq(1).removeClass('hideipt');

		} else {
			t.nextAll().eq(1).addClass('hideipt').val("");
		}
	});

	// Animate fillers on window scroll
	$(window).scroll(function(){
		var t = $(this);
		var s = t.scrollTop();
		var an = $('#animate');

		if(s >= an.offset().top - 100) {
			$(".c_fill").each(function() {
				var t = $(this);
			    var w = t.data("fill");
			    
			    t.animate({'width':w+'%'}, 400, function(){
			    	t.stop(true, true);
			    });
			});

		}
	});
});